#!/usr/bin/env python3

from ics import Calendar
import requests
import arrow
from dateutil.tz import gettz
import re
from collections import namedtuple
import itertools
import sys
from datetime import timedelta
import os
import json
import io

with open("config.json","r") as f:
    config = json.loads(''.join(f.readlines()))

WIKI_BASE_URL = config["wiki_base_url"]
meeting_url = config["meeting_url"]
event_url   = config["event_url"]
user = config["owncloud_user"]
pw = config["owncloud_password"]
meeting_cal_path = config["meeting_cal_path"]
event_cal_path = config["event_cal_path"]
html_output_path = config["html_output_path"]

outf = io.StringIO()
SECONDS_IN_A_DAY = 24*60*60
LinkElement = namedtuple('LinkElement', ['url','text'])

def getlink(text):
    m = re.fullmatch("(.*)\[\[(.*)\]\](.*)", text)
    if m:
        return LinkElement(WIKI_BASE_URL+m.group(2), ''.join(m.groups()))
    # otherwise return None


session = requests.session()
session.auth = (user, pw)

meetingcaltext = session.get(meeting_url).text
eventcaltext   = session.get(event_url  ).text

with open(meeting_cal_path+".tmp","w") as f:
    f.write(meetingcaltext)
os.rename(meeting_cal_path+".tmp",meeting_cal_path)
with open(event_cal_path+".tmp","w") as f:
    f.write(eventcaltext)
os.rename(event_cal_path+".tmp",event_cal_path)


meeting_cal = Calendar(meetingcaltext)
for event in meeting_cal.events:
    setattr(event, "important", False)
event_cal   = Calendar(eventcaltext)
for event in event_cal.events:
    setattr(event, "important", True)

now = arrow.utcnow()

# future_events = sorted(filter(lambda ev: ev.end > now, itertools.chain(meeting_cal.events,event_cal.events)))
future_events = sorted(filter(lambda ev: ev.end > now, event_cal.events))

if len(future_events) == 0:
    # truncate the output file
    with open(html_output_path,"w") as _:
        pass
    sys.exit(0)

print("""<div style="float:right; width:33%; min-width:300px; background:#ffe; border: 2px solid #000; border-radius:20px; color:#000; padding:1em; padding-top:0; margin-left:1em;">
<h2>Nächste Termine</h2>""", file=outf)

for event in future_events:
    name = event.name
    namelink = getlink(name)
    # TODO tidy up the html and css generation with an actual html builder
    if event.important:
        container_start="""<div style="background:linear-gradient(to right, rgba(255,0,0,0.2),transparent); border-left:5px solid red; padding: 0.5em; padding-top:0;"""+ ("cursor:pointer" if namelink else "") +""""%s>"""
    else:
        container_start="""<div style="background:linear-gradient(to right, rgba(0,0,0,0.2),transparent); border-left:5px solid black; padding: 0.5em; padding-top:0;"""+ ("cursor:pointer" if namelink else "") +""""%s>"""
    if namelink:
        name="""<a href="%s" style="text-decoration:none; color:black;">%s</a>""" % (namelink.url,namelink.text)
        container_start = container_start % (""" onclick="document.location.href = '%s';" """ % namelink.url)
    else:
        container_start = container_start % ""
    print(container_start, file=outf)
    print("""<h3>%s</h3>""" % name, file=outf)
    begin = event.begin.to("Europe/Berlin")
    end   = event.end.to("Europe/Berlin")
    if event.description:
        print(event.description,"<br>", file=outf)
    has_time = not (event.all_day or (event.end - event.begin).total_seconds() % SECONDS_IN_A_DAY == 0)
    has_end_date = event.end - event.begin > timedelta(days=1)
    if not has_time and not has_end_date:
        print("""<span style="display:inline-block"><b>Datum:</b> %s,</span>""" % begin.format("dddd D.M.YYYY","de_DE"), file=outf)
    elif has_time and not has_end_date:
        print("""<span style="display:inline-block"><b>Datum:</b> %s,</span>""" % begin.format("dddd D.M.YYYY","de_DE"), file=outf)
        print("""<span style="display:inline-block"><b>Beginn:</b> %s Uhr,</span>""" % begin.format("HH:mm"), file=outf)
        print("""<span style="display:inline-block"><b>Ende:</b> %s Uhr,</span>""" % end.format("HH:mm"), file=outf)
    elif not has_time and has_end_date:
        print("""<span style="display:inline-block"><b>Datum:</b> %s - %s,</span>""" % (begin.format("D.M." if end.year == begin.year else "D.M.YYYY","de_DE"), (end - timedelta(days=1)).format("D.M.YYYY","de_DE")), file=outf)
    else: # has_time and has_end_date
        print("""<span style="display:inline-block"><b>Datum:</b> %s Uhr bis %s Uhr,</span>""" % (begin.format("D.M.YYYY HH:mm"), end.format("D.M.YYYY HH:mm")), file=outf)
    if event.location:
        loc = event.location
        loclink = getlink(loc)
        if loclink:
            loc = """<a href="%s">%s</a>""" % (loclink.url,loclink.text)
        print("""<span style="display:inline-block"><b>Ort:</b> %s</span>""" % loc, file=outf)
    print("""</div>""", file=outf)


print("""</div>""", file=outf)

with open(html_output_path+".tmp","w") as f:
    f.write(outf.getvalue())
os.rename(html_output_path+".tmp",html_output_path)
